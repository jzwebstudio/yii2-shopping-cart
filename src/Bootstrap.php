<?php

/**
 * This file is part of the yii2-shopping-cart.
 *
 * Copyright 2021 Green Wave Palace Ltd. <jzwebstudio@gmail.com>.
 *
 * This source file is subject to the Commercial license that is bundled
 * with this source code in the file LICENSE.
 * @package yii2-shopping-cart
 */

namespace JzWebstudio\Yii2ShoppingCart;

/**
 * Description of Bootstrap
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class Bootstrap implements \yii\base\BootstrapInterface
{

    /**
     * @param \yii\base\Application $app
     */
    public function bootstrap($app)
    {
        $app->urlManager->addRules([
            [
                'class'=> \yii\web\GroupUrlRule::class,
                'prefix' => 'cart',
                'rules' => [
                    'GET ' => 'front/view',
                    'POST update' => 'front/update',
                    'delete' => 'front/delete',
                    'remove-item/<uid:[\w\-\.]+>' => 'front/remove',
                ]
            ]
        ],false);
    }

}
