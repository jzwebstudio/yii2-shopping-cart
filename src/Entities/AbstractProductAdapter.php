<?php

/**
 * This file is part of the yii2-shopping-cart.
 *
 * Copyright 2021 Green Wave Palace Ltd. <jzwebstudio@gmail.com>.
 *
 * This source file is subject to the Commercial license that is bundled
 * with this source code in the file LICENSE.
 * @package yii2-shopping-cart
 */

namespace JzWebstudio\Yii2ShoppingCart\Entities;

use RobotE13\ShoppingCart\Entities\Sellable;

/**
 * Description of AbstractProductAdapter
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
abstract class AbstractProductAdapter implements Sellable
{

    public function __construct(string $uid, float $quantity)
    {
        
    }

}
