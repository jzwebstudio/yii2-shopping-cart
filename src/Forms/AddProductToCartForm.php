<?php

/**
 * This file is part of the yii2-shopping-cart.
 *
 * Copyright 2021 Green Wave Palace Ltd. <jzwebstudio@gmail.com>.
 *
 * This source file is subject to the Commercial license that is bundled
 * with this source code in the file LICENSE.
 * @package yii2-shopping-cart
 */

namespace JzWebstudio\Yii2ShoppingCart\Forms;

use JzWebstudio\Yii2ShoppingCart\Entities\AbstractProductAdapter;

/**
 * Description of AddProductToCartForm
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class AddProductToCartForm extends \yii\base\Model
{

    public $skuNumber;
    public $quantity;

    public function rules(): array
    {
        return[
            [['skuNumber', 'quantity'], 'required'],
            [['skuNumber'], 'string'],
            [['quantity'], 'number', 'min' => 1]
        ];
    }

    public function getProduct(): AbstractProductAdapter
    {
        return \Yii::createObject(AbstractProductAdapter::class, [
                    $this->skuNumber,
                    $this->quantity
        ]);
    }

    public function formName()
    {
        return '';
    }

}
