<?php

/**
 * This file is part of the yii2-shopping-cart.
 *
 * Copyright 2021 Green Wave Palace Ltd. <jzwebstudio@gmail.com>.
 *
 * This source file is subject to the Commercial license that is bundled
 * with this source code in the file LICENSE.
 * @package yii2-shopping-cart
 */

namespace JzWebstudio\Yii2ShoppingCart\Controllers;

use Yii;
use JzWebstudio\Yii2ShoppingCart\Forms\AddProductToCartForm;
use RobotE13\ShoppingCart\Repositories\NotFoundException;
use RobotE13\ShoppingCart\Services\Cart\Serializer;
use RobotE13\ShoppingCart\Services\Cart\{
    AddItem\AddItem,
    Destroy\DestroyCart,
    Get\GetCart,
    RemoveItem\RemoveItem
};

/**
 * Description of FrontController
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class FrontController extends \yii\web\Controller
{

    public $enableCsrfValidation = false;

    public function behaviors()
    {
        return[
            'ajaxFilter' => [
                'class' => \yii\filters\AjaxFilter::class
            ],
            'verbFilter' => [
                'class' => \yii\filters\VerbFilter::class,
                'actions' => [
                    'update' => ['POST'],
                    'delete' => ['POST']
                ]
            ]
        ];
    }

    public function beforeAction($action)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return parent::beforeAction($action);
    }

    public function afterAction($action, $result)
    {
        return $this->serialize(parent::afterAction($action, $result));
    }

    public function actionView()
    {
        try {
            $command = new GetCart();
            return new Serializer($this->module->getCommandBus()->handle($command));
        } catch (NotFoundException $exc) {
            throw new \yii\web\NotFoundHttpException($exc->getMessage());
        }
    }

    public function actionUpdate()
    {
        $form = new AddProductToCartForm();
        $form->load(Yii::$app->request->post());
        if($form->validate())
        {
            $command = new AddItem([$form->getProduct()]);
            $cart = $this->module->getCommandBus()->handle($command);
            return new Serializer($cart);
        } else
        {
            return $form;
        }
    }

    public function actionRemove($uid)
    {
        $command = new RemoveItem($uid);
        return new Serializer($this->module->getCommandBus()->handle($command));
    }

    public function actionDelete()
    {
        $command = new DestroyCart();
        return new Serializer($this->module->getCommandBus()->handle($command));
    }

    protected function serialize($data)
    {
        return \Yii::createObject(\yii\rest\Serializer::class)->serialize($data);
    }

}
