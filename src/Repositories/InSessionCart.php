<?php

/**
 * This file is part of the yii2-shopping-cart.
 *
 * Copyright 2021 Green Wave Palace Ltd. <jzwebstudio@gmail.com>.
 *
 * This source file is subject to the Commercial license that is bundled
 * with this source code in the file LICENSE.
 * @package yii2-shopping-cart
 */

namespace JzWebstudio\Yii2ShoppingCart\Repositories;

use Yii;
use samdark\hydrator\Hydrator;
use RobotE13\ShoppingCart\Repositories\CartRepository;
use RobotE13\ShoppingCart\Repositories\NotFoundException;
use RobotE13\ShoppingCart\Entities\{
    Cart,
    Item
};

/**
 * Description of InSessionCart
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class InSessionCart implements CartRepository
{

    const CART_SESSION_KEY = 'robote13_shopping_cart';

    /**
     *
     * @var \samdark\hydrator\Hydrator
     */
    private $hydrator;

    public function __construct()
    {
        $this->hydrator = new Hydrator([
            'uid' => 'uid',
            'title' => 'title',
            'quantity' => 'quantity',
            'price' => 'price',
            'attributes' => 'attributes',
        ]);
    }

    public function get(): Cart
    {
        $items = Yii::$app->session->get(self::CART_SESSION_KEY);
        if($items === null)
        {
            throw new NotFoundException('Cart not exist.');
        }
        return new Cart(array_map(fn($item) => $this->hydrator->hydrate($item, Item::class), json_decode($items,true)));
    }

    public function put(Cart $cart)
    {
        $items = array_map(fn($item) => $this->hydrator->extract($item), $cart->getItems());
        Yii::$app->session->set(self::CART_SESSION_KEY, json_encode($items));
    }

    public function remove(): Cart
    {
        $cart = $this->get();
        Yii::$app->session->remove(self::CART_SESSION_KEY);
        return $cart;
    }

}
