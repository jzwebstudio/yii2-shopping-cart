<?php

/**
 * This file is part of the yii2-shopping-cart.
 *
 * Copyright 2021 Green Wave Palace Ltd. <jzwebstudio@gmail.com>.
 *
 * This source file is subject to the Commercial license that is bundled
 * with this source code in the file LICENSE.
 * @package yii2-shopping-cart
 */

namespace JzWebstudio\Yii2ShoppingCart;

use Yii;
use League\Tactician\CommandBus;
use League\Tactician\Container\ContainerLocator;
use League\Tactician\Handler\CommandHandlerMiddleware;
use League\Tactician\Handler\MethodNameInflector\HandleInflector;
use League\Tactician\Handler\CommandNameExtractor\ClassNameExtractor;
use RobotE13\ShoppingCart\Services\Cart\{
    AddItem\AddItem,
    AddItem\AddItemHandler,
    Get\GetCart,
    Get\GetCartHandler,
    Destroy\DestroyCart,
    Destroy\DestroyCartHandler,
    RemoveItem\RemoveItem,
    RemoveItem\RemoveItemHandler,
};

/**
 * Description of Module
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class Module extends \yii\base\Module
{

    public $defaultRoute = 'default';

    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'JzWebstudio\Yii2ShoppingCart\Controllers';

    /**
     *
     * @var CommandBus
     */
    private $commandBus;

    public function init()
    {
        parent::init();
        $this->commandBus = $this->createCommandBus();
    }

    public function getCommandBus(): CommandBus
    {
        return $this->commandBus;
    }


    final protected function createCommandBus($middleware = [])
    {
        $container = Yii::$container;
        $locator = new ContainerLocator($container, [
            AddItem::class => AddItemHandler::class,
            GetCart::class => GetCartHandler::class,
            DestroyCart::class=> DestroyCartHandler::class,
            RemoveItem::class=> RemoveItemHandler::class,
        ]);

        return new CommandBus(array_merge($middleware,[
            new CommandHandlerMiddleware(
                    new ClassNameExtractor(),
                    $locator,
                    new HandleInflector()
            )
        ]));
    }

}
