Storage management for web applications
=============================================
Yii2 implementation of robote13\storage-accounting

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist jzwebstudio\yii2-storage-accounting "*"
```

or add

```
"jzwebstudio\yii2-storage-accounting": "*"
```

to the require section of your `composer.json` file.


Usage
-----